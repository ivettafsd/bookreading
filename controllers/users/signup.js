const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { HttpError } = require('../../helpers');
const { User } = require('../../models/user');
const { SECRET_KEY, REFRESH_SECRET_KEY } = process.env;

const signup = async (req, res) => {
  const { name, email, password } = req.body;
  const foundUser = await User.findOne({ email });
  if (foundUser) {
    throw HttpError(409, 'Such email already exists');
  }
  const hashPassword = await bcrypt.hash(password, 10);
  const newUser = {
    email,
    name,
    password: hashPassword,
  };
  const user = await User.create(newUser);

  const payload = { id: user._id };

  const token = jwt.sign(payload, SECRET_KEY, { expiresIn: '1h' });
  const refreshToken = jwt.sign(payload, REFRESH_SECRET_KEY, { expiresIn: '7d' });

  await User.findByIdAndUpdate(user._id, { token, refreshToken });
  res.status(201).json({ email: user.email, name: user.name, token, refreshToken });
};

module.exports = signup;
