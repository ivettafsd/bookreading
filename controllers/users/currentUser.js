const currentUser = async (req, res) => {
  const { _id, name, email, token, refreshToken } = req.user;
  res.json({ _id, name, email, token, refreshToken });
};

module.exports = currentUser;
