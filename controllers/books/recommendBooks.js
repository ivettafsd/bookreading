const { HttpError } = require('../../helpers');
const { Book } = require('../../models/book');

const recommendBooks = async (req, res) => {
  const { title, author, page = 1, limit = 10 } = req.query;

  const query = { recommend: true };
  title && (query.title = { $regex: title, $options: 'i' });
  author && (query.author = { $regex: author, $options: 'i' });

  const skip = (page - 1) * limit;

const foundBooks = await Book.aggregate([
  { $match: query },
  {
    $facet: {
       metadata: [
          { $count: 'totalCount' }, 
          { $addFields: { page: Number(page), perPage: Number(limit) } }, 
          { $addFields: { totalPages: { $ceil: { $divide: ['$totalCount', '$perPage'] } } } }, 
        ],
      results: [
        { $skip: Number(skip) },
        { $limit: Number(limit) },
        { $unset: ['createdAt', 'updatedAt', 'owner'] },
      ],
    },
  },
  {
  $project: {
        totalPages: { $arrayElemAt: ['$metadata.totalPages', 0] }, 
        page: { $arrayElemAt: ['$metadata.page', 0] }, 
        perPage: { $arrayElemAt: ['$metadata.perPage', 0] }, 
        results: 1,
      },
  },
]);

  if (!foundBooks) {
    throw HttpError(404, 'Not found');
  }

  res.status(200).json(foundBooks[0]);
};

module.exports = recommendBooks;
