const { HttpError } = require('../../helpers');
const { Book } = require('../../models/book');

const addBook = async (req, res) => {
  const { title, author, totalPages } = req.body;
  const {_id} = req.user;

  const foundOwnBook = await Book.findOne({ title, owner: _id });
  
  if (foundOwnBook) {
    throw HttpError(409, 'Such book already exists');
  }

  const newBook = {
    title,
    author,
    totalPages,
    imageUrl: req.file ? req.file.path : null,
    status: 'unread',
    owner: _id
  };
  const createdBook = await Book.create(newBook);

  res.status(201).json({title: createdBook.title, author: createdBook.author, imageUrl: createdBook.imageUrl, totalPages: createdBook.totalPages, status: createdBook.status, owner: createdBook.owner, _id: createdBook._id, progress: createdBook.progress});
};

module.exports = addBook;