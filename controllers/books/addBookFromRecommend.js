const { HttpError } = require('../../helpers');
const { Book } = require('../../models/book');

const addBookFromRecommend = async (req, res) => {
  const { id: bookId} = req.params;
  const {_id} = req.user;

  const foundBook = await Book.findOne({ _id: bookId });

  if (!foundBook) {
    throw HttpError(404, 'Not found');
  }

  if (foundBook.owner && _id == foundBook.owner) {
    throw HttpError(409, 'Such book already exists');
  }

  const newBook = {
    title: foundBook.title,
    author: foundBook.author,
    imageUrl: foundBook.imageUrl,
    totalPages: foundBook.totalPages,
    status: 'unread',
    owner: _id
  };
  const createdBook = await Book.create(newBook);
  res.status(201).json(createdBook);
};

module.exports = addBookFromRecommend;