const { HttpError, countLeftTime } = require('../../helpers');
const { Book } = require('../../models/book');

const finishReadingBook = async (req, res) => {
  const { id: bookId, page: finishPage } = req.body;
  const finishReading = new Date();
  const { _id: userId } = req.user;

  const foundBook = await Book.findOne({ _id: bookId, owner: userId });

  if (!foundBook) {
    throw HttpError(404, 'Not found');
  }
  if (foundBook.status === 'unread') {
    throw HttpError(409, "You haven't started reading this book");
  }
  if (foundBook.status === 'done') {
    throw HttpError(409, 'This book is already read');
  }

  const activeReading = foundBook.progress.find((i) => i.status === 'active');
  if (!activeReading) {
    throw HttpError(409, "You haven't started reading this book");
  }

  const checkBookStatus = foundBook.totalPages <= finishPage ? 'done' : 'in-progress';

  const { startPage, startReading, status } = activeReading;
  if (finishPage < startPage) {
    throw HttpError(409, "The finish page cann't be less than the start page");
  }

  const totalPagesRead = finishPage - startPage;
  const totalTimeRead = finishReading - startReading;
  const speed = totalPagesRead / totalTimeRead;
  const speedInHours = Math.round(speed * 3600000);

  await Book.findByIdAndUpdate(foundBook._id, { $pull: { progress: { status: 'active' } } }, { new: true });

  const updatedBook = await Book.findByIdAndUpdate(
    foundBook._id,
    {
      status: checkBookStatus,
      $push: { progress: { startPage, startReading, status, finishPage, finishReading: new Date(), status: 'inactive', speed: speedInHours } },
    },
    { new: true },
  ).select('-createdAt -updatedAt -recommend');

  const totalPagesToRead = foundBook.totalPages - finishPage;

  res.status(200).json({ ...updatedBook.toObject(), timeLeftToRead: countLeftTime(updatedBook, totalPagesToRead) });
};

module.exports = finishReadingBook;
