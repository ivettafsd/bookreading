const { Book } = require('../../models/book');
const { HttpError } = require('../../helpers');

const removeBook = async (req, res) => {
  const { _id: userId } = req.user;
  const { id: bookId } = req.params;

  const foundBook = await Book.findOneAndRemove({ _id: bookId, owner: userId });
  if (!foundBook) {
    throw HttpError(401, 'This book not found');
  }

  const result = {
    message: 'This book was deleted',
    id: bookId,
  };
  res.json(result);
};

module.exports = removeBook;
