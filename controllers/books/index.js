const { CtrlWrapper } = require('../../helpers');
const recommendBooks = require('./recommendBooks');
const getBookById = require('./getBookById');
const addBook = require('./addBook');
const addBookFromRecommend = require('./addBookFromRecommend');
const ownBooks = require('./ownBooks');
const startReadingBook = require('./startReadingBook');
const finishReadingBook = require('./finishReadingBook');
const deleteReadingBook = require('./deleteReadingBook');
const removeBook = require('./removeBook')

module.exports = {
   recommendBooks: CtrlWrapper(recommendBooks),
   getBookById: CtrlWrapper(getBookById),
   addBook: CtrlWrapper(addBook),
   removeBook: CtrlWrapper(removeBook),
   addBookFromRecommend: CtrlWrapper(addBookFromRecommend),
   ownBooks: CtrlWrapper(ownBooks),
   startReadingBook: CtrlWrapper(startReadingBook),
   finishReadingBook: CtrlWrapper(finishReadingBook),
   deleteReadingBook: CtrlWrapper(deleteReadingBook)
};
