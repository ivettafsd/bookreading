const { HttpError, countLeftTime } = require('../../helpers');
const { Book } = require('../../models/book');

const getBookById = async (req, res) => {
  const { id: bookId } = req.params;
  const { _id: userId } = req.user;

  const foundBook = await Book.findById(bookId).select('-createdAt -updatedAt -recommend');

  if (!foundBook) {
    throw HttpError(404, 'Not found');
  }

  if (foundBook.owner.toString() !== userId.toString()) {
    const { progress, ...book } = foundBook.toObject();
    res.status(200).json(book);
  } else {
    if (foundBook.status !== 'unread') {
      const totalPagesToRead = foundBook.totalPages - foundBook.progress[foundBook.progress.length - 1].finishPage;
      res.status(200).json({ ...foundBook.toObject(), timeLeftToRead: countLeftTime(foundBook, totalPagesToRead) });
    } else {
      res.status(200).json({ ...foundBook.toObject() });
    }
  }
};

module.exports = getBookById;
