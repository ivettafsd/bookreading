const { HttpError, countLeftTime } = require('../../helpers');
const { Book } = require('../../models/book');

const deleteReadingBook = async (req, res) => {
  const { bookId, readingId } = req.query;
  const { _id: userId } = req.user;

  const foundBook = await Book.findOne({ _id: bookId, owner: userId });

  if (!foundBook) {
    throw HttpError(401, 'Info about this book not found');
  }
  const doneReading = foundBook.progress.find((reading) => reading._id.toString() === readingId);
  if (!doneReading) {
    throw HttpError(401, 'Info about this book not found');
  }
  if (foundBook.status === 'done') {
    throw HttpError(409, 'This book is already read');
  }
  if (foundBook.progress.find((i) => i.status === 'active')) {
    throw HttpError(409, "You haven't finished reading this book");
  }

  const updatedBook = await Book.findByIdAndUpdate(
    bookId,
    {
      status: foundBook.progress.length >1 ? 'in-progress' : 'unread',
      $pull: { progress: { _id: readingId } },
    },
    { new: true },
  ).select('-createdAt -updatedAt -recommend');

  if (updatedBook.status !== 'unread') {
    const totalPagesToRead = updatedBook.totalPages - updatedBook.progress[updatedBook.progress.length - 1].finishPage;
    res.status(200).json({ ...updatedBook.toObject(), timeLeftToRead: countLeftTime(updatedBook, totalPagesToRead) });
  } else {
    res.status(200).json(updatedBook);
  }
};

module.exports = deleteReadingBook;
