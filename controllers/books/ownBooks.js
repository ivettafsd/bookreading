const { HttpError } = require('../../helpers');
const { Book } = require('../../models/book');

const ownBooks = async (req, res) => {
  const { status } = req.query;
  const { _id } = req.user;

  const query = { owner: _id };
  status && (query.status = status);

  const foundBooks = await Book.find(query).select('-recommend -createdAt -updatedAt');

  if (!foundBooks) {
    throw HttpError(404, 'Not found');
  }

  res.status(200).json(foundBooks);
};

module.exports = ownBooks;
