const { HttpError } = require('../../helpers');
const { Book } = require('../../models/book');

const startReadingBook = async (req, res) => {
  const { id: bookId, page } = req.body;
    const { _id: userId } = req.user;
    const startReading = new Date();

  const foundBook = await Book.findOne({ _id: bookId, owner: userId });

  if (!foundBook) {
    throw HttpError(404, 'Not found');
  }
  if (foundBook.status === 'done') {
    throw HttpError(409, 'This book is already read');
  }
  if (foundBook.progress.find((i) => i.status === 'active')) {
    throw HttpError(409, 'You have already started reading this book');
  }

  const updatedBook = await Book.findByIdAndUpdate(
    foundBook._id,
    {
      status: 'in-progress',
      $push: { progress: { startPage: page, startReading, status: 'active' } },
    },
    { new: true },
  ).select('-createdAt -updatedAt -recommend');

  res.status(200).json(updatedBook);
};

module.exports = startReadingBook;
