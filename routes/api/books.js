const express = require('express');
const ctrl = require('../../controllers/books');
const { validateBody, authenticate, upload, validateId, validateQuery } = require('../../middlewares');
const { schemas } = require('../../models/book');
const router = express.Router();
router.get('/recommend', authenticate, ctrl.recommendBooks);
router.post('/add', authenticate, upload.single('image'), validateBody(schemas.addBookSchema), ctrl.addBook);
router.post('/add/:id', authenticate, validateId, ctrl.addBookFromRecommend);
router.delete('/remove/:id', authenticate, validateId, ctrl.removeBook);
router.get('/own', authenticate, validateQuery(schemas.searchOwnBooksByStatus), ctrl.ownBooks);
router.post('/reading/start', authenticate, validateBody(schemas.readingBookSchema), ctrl.startReadingBook);
router.post('/reading/finish', authenticate, validateBody(schemas.readingBookSchema), ctrl.finishReadingBook);
router.delete('/reading', authenticate, validateQuery(schemas.deleteReadingBookSchema), ctrl.deleteReadingBook);
router.get('/:id', authenticate, validateId, ctrl.getBookById);


module.exports = router;