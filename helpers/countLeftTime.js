const countLeftTime = (book, totalPagesToRead) => {
  const averageSpeed =
book.progress.reduce((acc, item) => {
      acc = acc + item.speed / 3600000;
      return acc;
    }, 0) / book.progress.length;

  const timeLeftToRead = totalPagesToRead / averageSpeed / 1000;

  const leftHours = Math.round(timeLeftToRead / 3600);
  const leftMinutes = Math.round((timeLeftToRead % 3600) / 60);
  const leftSeconds = Math.round((timeLeftToRead % 3600) % 60);

return { hours: leftHours, minutes: leftMinutes, seconds: leftSeconds }
}

module.exports = countLeftTime;