const { handleMangooseError, HttpError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const bookSchema = Schema(
  {
    title: {
      type: String,
      required: [true, 'Set book title'],
    },
    author: {
      type: String,
      required: [true, 'Set book author'],
    },
    imageUrl: {
      type: String || null,
    },
    totalPages: {
      type: Number,
      required: [true, 'Set total pages'],
    },
    status: {
      type: String,
      enum: ['unread', 'in-progress', 'done'],
    },
    recommend: {
      type: Boolean,
      default: false,
    },
    progress: {
      type: [
        {
          startPage: {
            type: Number,
            required: false,
          },
          startReading: {
            type: Date,
            required: false,
          },
          finishPage: {
            type: Number,
            required: false,
          },
          finishReading: {
            type: Date,
            required: false,
          },
          speed: {
            type: Number,
            required: false,
          },
          status: {
            type: String,
            enum: ['active', 'inactive'],
            speed: Number,
          },
        },
      ],
      default: [],
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

bookSchema.post('save findByIdAndUpdate', handleMangooseError);

const addBookSchema = Joi.object({
  title: Joi.string().required().empty(false).messages({
    'string.base': 'The title must be a string.',
    'any.required': 'The title field is required.',
    'string.empty': 'The title must not be empty.',
  }),
  author: Joi.string().required().empty(false).messages({
    'string.base': 'The author must be a string.',
    'any.required': 'The author field is required.',
    'string.empty': 'The author must not be empty.',
  }),
  totalPages: Joi.number().required().min(1).messages({
    'number.base': 'The total pages must be a number.',
    'any.required': 'The total pages field is required.',
    'number.min': 'The total pages must be greater than or equal to 1.',
  }),
});

const searchOwnBooksByStatus = Joi.object({
  status: Joi.string().valid('unread', 'in-progress', 'done').messages({
    'string.base': 'The status field must be a string',
    'any.only': "Invalid status. Allowed values are 'unread', 'in-progress', 'done'",
  }),
});
const deleteReadingBookSchema = Joi.object({
  bookId: Joi.string().required().empty(false).messages({
    'string.base': 'The bookId must be a string.',
    'any.required': 'The bookId field is required.',
    'string.empty': 'The bookId must not be empty.',
  }),
  readingId: Joi.string().required().empty(false).messages({
    'string.base': 'The readingId must be a string.',
    'any.required': 'The readingId field is required.',
    'string.empty': 'The readingId must not be empty.',
  }),
});

const readingBookSchema = Joi.object({
  id: Joi.string().required().empty(false).messages({
    'string.base': 'The id must be a string.',
    'any.required': 'The id field is required.',
    'string.empty': 'The id must not be empty.',
  }),
  page: Joi.number().required().min(1).messages({
    'number.base': 'The page must be a number.',
    'any.required': 'The page field is required.',
    'number.min': 'The page must be greater than or equal to 1.',
  }),
});

const Book = model('book', bookSchema);
const schemas = { addBookSchema, searchOwnBooksByStatus, deleteReadingBookSchema, readingBookSchema };

module.exports = { Book, schemas };
